package ir.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.json.simple.parser.ParseException;

import ir.dao.BaseEntityDAO;
import ir.dao.UserDAO;
import ir.model.BaseEntity;
import ir.model.User;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public abstract class EntityCRUDServices<T extends BaseEntity> {
	private static final String SECURITY_HEADER_KEY = "x-access-token";

	public abstract BaseEntityDAO<T> getDAO();

	public void validateAdd(T item) throws BadRequestException {
		if (!StringUtils.isBlank(item.getUid()))
			throw new BadRequestException("Uid is not null");
	}

	public void validateEdit(T item) throws BadRequestException {
		if (StringUtils.isBlank(item.getUid())) {
			throw new BadRequestException("Uid is null");
		}
		if (getDAO().getByUid(item.getUid()) == null)
			throw new BadRequestException("Entity not exist with this uid");
	}

	protected User getAttachedUser(HttpHeaders headers) throws NotAuthorizedException {
		if (!headers.getRequestHeaders().containsKey(SECURITY_HEADER_KEY)) {
			throw new NotAuthorizedException("Request does not contain security header");
		}
		String requestToken = headers.getRequestHeader(SECURITY_HEADER_KEY).get(0);
		User user = UserDAO.getInstance().getByToken(requestToken);
		if (user == null) {
			throw new NotAuthorizedException("you are not logged in...");
		}
		return user;
	}

	@POST
	@Path("/add")
	public T add(T entity, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException, BadRequestException {
		getAttachedUser(headers);
		validateAdd(entity);
		return getDAO().AddOrUpdate(entity);
	}

	@POST
	@Path("/edit")
	public T edit(T entity, @Context HttpHeaders headers) throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		validateEdit(entity);
		return getDAO().AddOrUpdate(entity);
	}

	@POST
	@Path("/delete")
	public void delete(@QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		getDAO().delete(uid);
	}

	@GET
	public T get(@QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		return getDAO().getByUid(uid);
	}

	@GET
	@Path("/list")
	public ArrayList<T> getList(@Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		return getDAO().getItems();
	}
}
