package ir.util;

import java.util.ArrayList;
import java.util.Date;

import ir.dao.ProcessInstanceDAO;
import ir.dao.ProcessStepInstanceDAO;
import ir.dao.ProcessTypeDAO;
import ir.dao.RoleDAO;
import ir.dao.UserDAO;
import ir.model.ProcessStepType;
import ir.model.ProcessType;
import ir.model.Role;
import ir.model.User;

public class InitDB {
	public static void initUsers() {
		initUser("admin");
		initUser("sysadmin");
		initUser("student");
		initUser("library");
		initUser("education");
		initUser("hospital");
	}

	public static void initRoles() {
		initRole("admin");
		initRole("student");
		initRole("sysadmin");
		initRole("library");
		initRole("education");
		initRole("hospital");

	}

	public static void initProcessType() {
		ProcessType type = new ProcessType();
		type.setName("فراغت از تحصیل ورودی ۹۲");
		type.setStartStepUid("1");
		ProcessStepType step1 = new ProcessStepType();
		step1.setUid("1");
		step1.setName("تایید اطلاعات دانشجو");
		step1.setOnAcceptStep("2");
		step1.setOnRejectStep("3");
		step1.setRole(RoleDAO.getInstance().getByName("student"));

		ProcessStepType step2 = new ProcessStepType();
		step2.setUid("2");
		step2.setName("تصویه کتابخانه");
		step2.setOnAcceptStep("4");
		step2.setOnRejectStep("2");
		step2.setRole(RoleDAO.getInstance().getByName("library"));

		ProcessStepType step3 = new ProcessStepType();
		step3.setUid("3");
		step3.setName("ویرایش مشخصات دانشجو");
		step3.setOnAcceptStep("2");
		step3.setOnRejectStep("3");
		step3.setRole(RoleDAO.getInstance().getByName("education"));

		ProcessStepType step4 = new ProcessStepType();
		step4.setUid("4");
		step4.setName("تسویه بیمارستان");
		step4.setOnAcceptStep("5");
		step4.setOnRejectStep("4");
		step4.setRole(RoleDAO.getInstance().getByName("hospital"));

		ProcessStepType step5 = new ProcessStepType();
		step5.setUid("5");
		step5.setName("تایید واحدها");
		step5.setOnAcceptStep("FINISH");
		step5.setOnRejectStep("5");
		step5.setRole(RoleDAO.getInstance().getByName("education"));

		ArrayList<ProcessStepType> steps = new ArrayList<>();
		steps.add(step1);
		steps.add(step2);
		steps.add(step3);
		steps.add(step4);
		steps.add(step5);

		type.setSteps(steps);
		ProcessTypeDAO.getInstance().AddOrUpdate(type);
	}

	private static void initRole(String name) {
		Role r = new Role();
		r.setName(name);
		RoleDAO.getInstance().AddOrUpdate(r);
	}

	private static void initUser(String info) {
		User user = new User();
		user.setBirthDate(new Date().getTime());
		user.setFirstName(info);
		user.setLastName(info);
		user.setNationalId(4610500299L);
		user.setPassword(info);
		user.setRole(RoleDAO.getInstance().getByName(info));
		user.setUsername(info);
		user.setToken(info);
		UserDAO.getInstance().AddOrUpdate(user);
	}

	public static void resetDb() {
		ProcessInstanceDAO.getInstance().drop();
		ProcessStepInstanceDAO.getInstance().drop();
		ProcessTypeDAO.getInstance().drop();
		RoleDAO.getInstance().drop();
		UserDAO.getInstance().drop();
	}
	public static void main(String[] args) {
		InitDB.resetDb();
		InitDB.initRoles();
		InitDB.initUsers();
		InitDB.initProcessType();

	}
}
