package ir.api;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Path;

import ir.dao.RoleDAO;
import ir.model.Role;
import ir.util.EntityCRUDServices;
import ir.util.StringUtils;

@Path("/roles")
public class RoleServices extends EntityCRUDServices<Role> {
	@Override
	public RoleDAO getDAO() {
		return RoleDAO.getInstance();
	}

	@Override
	public void validateAdd(Role item) throws BadRequestException {
		super.validateAdd(item);
		if (StringUtils.isBlank(item.getName()) || RoleDAO.getInstance().getByName(item.getName()) != null)
			throw new BadRequestException();

	}
}
