package ir.api;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Path;

import ir.dao.ProcessTypeDAO;
import ir.dao.RoleDAO;
import ir.model.ProcessStepType;
import ir.model.ProcessType;
import ir.util.EntityCRUDServices;
import ir.util.StringUtils;

@Path("/process_type")
public class ProcessTypeServices extends EntityCRUDServices<ProcessType> {

	@Override
	public ProcessTypeDAO getDAO() {
		return ProcessTypeDAO.getInstance();
	}

	@Override
	public void validateAdd(ProcessType item) throws BadRequestException {
		super.validateAdd(item);
		validateAddOrUpdate(item);
	}

	@Override
	public void validateEdit(ProcessType item) throws BadRequestException {
		super.validateEdit(item);
		validateAddOrUpdate(item);
	}

	private void validateAddOrUpdate(ProcessType item) throws BadRequestException {
		if (StringUtils.isBlank(item.getName()) || StringUtils.isBlank(item.getStartStepUid())
				|| !item.hasStep(item.getStartStepUid()) || !item.hasFinishStep())
			throw new BadRequestException();
		if (item.getSteps() == null)
			throw new BadRequestException();
		for (ProcessStepType element : item.getSteps()) {
			if (StringUtils.isBlank(element.getName()) || StringUtils.isBlank(element.getOnAcceptStep())
					|| StringUtils.isBlank(element.getOnRejectStep()) || !item.hasStep(element.getOnAcceptStep())
					|| !item.hasStep(element.getOnRejectStep()) || element.getRole() == null
					|| StringUtils.isBlank(element.getRole().getUid())
					|| RoleDAO.getInstance().getByUid(element.getRole().getUid()) == null)
				throw new BadRequestException();
		}
	}

}
