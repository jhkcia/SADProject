package ir.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.util.InitDB;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/")
public class TestServices {
	public static final int MAIN_VERSION = 1;
	public static final int MINOR_VERSION = 0;
	public static final int MIRROR_VERSION = 1;

	public static final String VERSION = MAIN_VERSION + "." + MINOR_VERSION + "." + MIRROR_VERSION;

	@GET
	@Path("/test")
	public String test() {
		return "{\"info\":\"I am Live in version  " + VERSION + "\"}";
	}

	@GET
	@Path("/reset")
	public String reset() {
		InitDB.resetDb();
		return "{\"state\":\"done\"}";
	}

	@GET
	@Path("/init")
	public String init() {
		InitDB.resetDb();
		InitDB.initRoles();
		InitDB.initUsers();
		InitDB.initProcessType();
		return "{\"state\":\"done\"}";
	}
}
