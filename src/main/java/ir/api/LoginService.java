package ir.api;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.parser.ParseException;

import ir.dao.UserDAO;
import ir.model.User;
import ir.util.StringUtils;
import ir.util.UidGenerator;

@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoginService {
	@GET
	public User login(@QueryParam("username") String username, @QueryParam("password") String password)
			throws FileNotFoundException, IOException, ParseException {
		if (StringUtils.isBlank(password) || StringUtils.isBlank(username)) {
			throw new BadRequestException();
		}

		User user = UserDAO.getInstance().getByUsername(username);
		if (user == null) {
			throw new NotFoundException(username + "not found");
		}
		if (!password.equals(user.getPassword()))
			throw new NotAuthorizedException("bad credential");
		String token = UidGenerator.generateUid();
		UserDAO.getInstance().setToken(username, token);
		System.out.println("User " + username + " successfully logged in");
		return UserDAO.getInstance().getByUsername(username);
	}

}
