package ir.api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.json.simple.parser.ParseException;

import ir.dao.ProcessInstanceDAO;
import ir.dao.ProcessStepInstanceDAO;
import ir.dao.ProcessTypeDAO;
import ir.model.Message;
import ir.model.ProcessInstance;
import ir.model.ProcessStepInstance;
import ir.model.ProcessStepType;
import ir.model.ProcessType;
import ir.model.User;
import ir.util.EntityCRUDServices;

@Path("/process_step_instance")
public class ProcessStepInstanceServices extends EntityCRUDServices<ProcessStepInstance> {

	@Override
	public ProcessStepInstanceDAO getDAO() {
		return ProcessStepInstanceDAO.getInstance();
	}

	@Override
	public ProcessStepInstance add(ProcessStepInstance entity, HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException, BadRequestException {
		throw new BadRequestException();
	}

	@Override
	public ProcessStepInstance edit(ProcessStepInstance entity, HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		throw new BadRequestException();
	}

	@Override
	public void delete(String uid, HttpHeaders headers) throws FileNotFoundException, IOException, ParseException {
		throw new BadRequestException();
	}

	@Override
	public ArrayList<ProcessStepInstance> getList(HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		User user = getAttachedUser(headers);
		if (user.getRole().getName().equals("sysadmin"))
			return getDAO().getItems();
		else if (user.getRole().getName().equals("student"))
			throw new BadRequestException();
		else {
			return getDAO().getOpenedSteps(user.getRole().getUid());
		}

	}

	@POST
	@Path("/accept")
	public ProcessStepInstance accept(@QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException, AccessDeniedException {
		User user = getAttachedUser(headers);
		ProcessStepInstance instance = ProcessStepInstanceDAO.getInstance().getByUid(uid);
		if (!user.getRole().getUid().equals(instance.getRole()))
			throw new AccessDeniedException(null);
		if (instance.getState().equals("CLOSED"))
			throw new BadRequestException();
		instance.setFinishDate(new Date().getTime());
		instance.setState("CLOSED");
		instance = getDAO().AddOrUpdate(instance);
		ProcessInstance processInstance = ProcessInstanceDAO.getInstance().getByUid(instance.getProcessInstance());
		ProcessStepType type = ProcessTypeDAO.getInstance().getByUid(processInstance.getProcessType())
				.getStep(instance.getProcessStepTypeUid());
		if (type.getOnAcceptStep().equalsIgnoreCase("FINISH")) {
			processInstance.setFinishDate(new Date().getTime());
			processInstance.setState("CLOSED");
			processInstance.setCurrentStepInstanceUid("FINISH");
			ProcessInstanceDAO.getInstance().AddOrUpdate(processInstance);
		} else {
			ProcessStepInstance newStep = new ProcessStepInstance();
			newStep.setCost(0L);
			newStep.setCreationDate(new Date().getTime());
			newStep.setProcessInstance(processInstance.getUid());
			newStep.setProcessStepTypeUid(type.getOnAcceptStep());
			newStep.setState("OPEN");
			ProcessType processType = ProcessTypeDAO.getInstance().getByUid(processInstance.getProcessType());
			newStep.setRole(processType.getStep(type.getOnAcceptStep()).getRole().getUid());
			newStep = ProcessStepInstanceDAO.getInstance().AddOrUpdate(newStep);
			processInstance.setCurrentStepInstanceUid(newStep.getUid());
			ProcessInstanceDAO.getInstance().AddOrUpdate(processInstance);

		}
		return instance;

	}

	@POST
	@Path("/reject")
	public ProcessStepInstance reject(@QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException, AccessDeniedException {
		User user = getAttachedUser(headers);
		ProcessStepInstance instance = ProcessStepInstanceDAO.getInstance().getByUid(uid);
		if (!user.getRole().getUid().equals(instance.getRole()))
			throw new AccessDeniedException(null);
		if (instance.getState().equals("CLOSED"))
			throw new BadRequestException();
		instance.setFinishDate(new Date().getTime());
		instance.setState("CLOSED");
		instance = getDAO().AddOrUpdate(instance);
		ProcessInstance processInstance = ProcessInstanceDAO.getInstance().getByUid(instance.getProcessInstance());
		ProcessStepType type = ProcessTypeDAO.getInstance().getByUid(processInstance.getProcessType())
				.getStep(instance.getProcessStepTypeUid());
		ProcessStepInstance newStep = new ProcessStepInstance();
		newStep.setCost(0L);
		newStep.setCreationDate(new Date().getTime());
		newStep.setProcessInstance(processInstance.getUid());
		newStep.setProcessStepTypeUid(type.getOnRejectStep());
		newStep.setState("OPEN");
		ProcessType processType = ProcessTypeDAO.getInstance().getByUid(processInstance.getProcessType());
		newStep.setRole(processType.getStep(type.getOnRejectStep()).getRole().getUid());
		newStep = ProcessStepInstanceDAO.getInstance().AddOrUpdate(newStep);
		processInstance.setCurrentStepInstanceUid(newStep.getUid());
		ProcessInstanceDAO.getInstance().AddOrUpdate(processInstance);
		return instance;
	}

	@POST
	@Path("/pay")
	public ProcessStepInstance pay(@QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		ProcessStepInstance instance = ProcessStepInstanceDAO.getInstance().getByUid(uid);
		instance.setCost(0L);
		return getDAO().AddOrUpdate(instance);
	}

	@POST
	@Path("/change_cost")
	public ProcessStepInstance changeCost(@QueryParam("uid") String uid, @QueryParam("cost") Long cost,
			@Context HttpHeaders headers) throws FileNotFoundException, IOException, ParseException {
		getAttachedUser(headers);
		ProcessStepInstance instance = ProcessStepInstanceDAO.getInstance().getByUid(uid);
		instance.setCost(cost);
		return getDAO().AddOrUpdate(instance);
	}

	@POST
	@Path("/sendMessage")
	public ProcessStepInstance sendMessage(Message msg, @QueryParam("uid") String uid, @Context HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		User user = getAttachedUser(headers);
		ProcessStepInstance instance = ProcessStepInstanceDAO.getInstance().getByUid(uid);
		ArrayList<Message> messages = instance.getMessages();
		msg.setSender(user.getUsername());
		messages.add(msg);
		instance.setMessages(messages);
		return getDAO().AddOrUpdate(instance);
	}
}
