package ir.api;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Path;

import ir.dao.RoleDAO;
import ir.dao.UserDAO;
import ir.model.User;
import ir.util.EntityCRUDServices;
import ir.util.StringUtils;

@Path("/users")
public class UserServices extends EntityCRUDServices<User> {

	@Override
	public UserDAO getDAO() {
		return UserDAO.getInstance();
	}

	@Override
	public void validateAdd(User item) throws BadRequestException {
		if (StringUtils.isBlank(item.getPassword()) || StringUtils.isBlank(item.getUsername()) || item.getRole() == null
				|| StringUtils.isBlank(item.getRole().getUid())) {
			throw new BadRequestException();
		}
		if (UserDAO.getInstance().getByUsername(item.getUsername()) != null) {
			throw new BadRequestException("User exists.");
		}
		if (RoleDAO.getInstance().getByUid(item.getRole().getUid()) == null)
			throw new BadRequestException("Role not exists.");
		super.validateAdd(item);
	}

	@Override
	public void validateEdit(User item) throws BadRequestException {
		if (UserDAO.getInstance().getByUsername(item.getUsername()) == null) {
			throw new BadRequestException("User not exists.");
		}
		if (StringUtils.isBlank(item.getPassword()) || StringUtils.isBlank(item.getUsername()) || item.getRole() == null
				|| StringUtils.isBlank(item.getRole().getUid())) {
			throw new BadRequestException();
		}
		if (RoleDAO.getInstance().getByUid(item.getRole().getUid()) == null)
			throw new BadRequestException("Role not exists.");
		super.validateEdit(item);
	}

}
