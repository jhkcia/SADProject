package ir.api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;

import org.json.simple.parser.ParseException;

import ir.dao.ProcessInstanceDAO;
import ir.dao.ProcessStepInstanceDAO;
import ir.dao.ProcessTypeDAO;
import ir.model.Message;
import ir.model.ProcessInstance;
import ir.model.ProcessStepInstance;
import ir.model.ProcessType;
import ir.model.User;
import ir.util.EntityCRUDServices;
import ir.util.StringUtils;

@Path("/process_instance")
public class ProcessInstanceServices extends EntityCRUDServices<ProcessInstance> {
	@Override
	public ProcessInstance add(ProcessInstance entity, HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException, BadRequestException {
		if (StringUtils.isBlank(entity.getProcessType())) {
			throw new BadRequestException();
		}
		User user = getAttachedUser(headers);
		if(getDAO().getByUserAndType(user.getUid(), entity.getProcessType())!=null){
			throw new BadRequestException();
		}
		entity.setCreationDate(new Date().getTime());
		entity.setState("OPEN");
		entity.setOwner(user.getUid());
		ProcessType processType = ProcessTypeDAO.getInstance().getByUid(entity.getProcessType());
		String firstStepUid = processType.getStartStepUid();
		validateAdd(entity);
		ProcessInstance dbObj = getDAO().AddOrUpdate(entity);
		ProcessStepInstance instance = new ProcessStepInstance();
		instance.setCost(0L);
		instance.setCreationDate(new Date().getTime());
		instance.setMessages(new ArrayList<Message>());
		instance.setProcessInstance(dbObj.getUid());
		instance.setRole(processType.getStep(firstStepUid).getRole().getUid());
		instance.setState("OPEN");
		instance.setProcessStepTypeUid(firstStepUid);

		instance = ProcessStepInstanceDAO.getInstance().AddOrUpdate(instance);
		dbObj.setCurrentStepInstanceUid(instance.getUid());
		return getDAO().AddOrUpdate(dbObj);
	}

	@Override
	public ProcessInstance edit(ProcessInstance entity, HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		throw new BadRequestException();
	}

	@Override
	public ArrayList<ProcessInstance> getList(HttpHeaders headers)
			throws FileNotFoundException, IOException, ParseException {
		User user = getAttachedUser(headers);
		return getDAO().getUserItems(user.getUid());
	}

	@Override
	public void delete(String uid, HttpHeaders headers) throws FileNotFoundException, IOException, ParseException {
		throw new BadRequestException();
	}

	@Override
	public ProcessInstanceDAO getDAO() {
		return ProcessInstanceDAO.getInstance();
	}
}
