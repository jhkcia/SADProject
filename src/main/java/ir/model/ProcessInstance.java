package ir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessInstance extends BaseEntity {
	private long creationDate;
	private long finishDate;
	private String currentStepInstanceUid;
	private String processType;
	private String owner;
	private String state;

	@JsonProperty
	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	@JsonProperty
	public long getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(long finishDate) {
		this.finishDate = finishDate;
	}

	@JsonProperty
	public String getCurrentStepInstanceUid() {
		return currentStepInstanceUid;
	}

	public void setCurrentStepInstanceUid(String currentStep) {
		this.currentStepInstanceUid = currentStep;
	}

	@JsonProperty
	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	@JsonProperty
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@JsonProperty
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


}
