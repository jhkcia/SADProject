package ir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
	private String sender;
	private String text;

	@JsonProperty
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	@JsonProperty
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
