package ir.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessStepInstance extends BaseEntity {
	private String processInstance;
	private Long cost;
	private String state;
	private ArrayList<Message> messages;
	private long creationDate;
	private long finishDate;
	private String role;
	private String processStepTypeUid;

	@JsonProperty
	public String getProcessStepTypeUid() {
		return processStepTypeUid;
	}

	public void setProcessStepTypeUid(String processStepTypeUid) {
		this.processStepTypeUid = processStepTypeUid;
	}

	@JsonProperty
	public String getProcessInstance() {
		return processInstance;
	}

	public void setProcessInstance(String processInstance) {
		this.processInstance = processInstance;
	}

	@JsonProperty
	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	@JsonProperty
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty
	public ArrayList<Message> getMessages() {
		if (messages == null)
			messages = new ArrayList<>();
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	@JsonProperty
	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	@JsonProperty
	public long getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(long finishDate) {
		this.finishDate = finishDate;
	}

	@JsonProperty
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
