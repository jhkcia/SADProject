package ir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class BaseEntity {
	private String uid;

	@JsonProperty
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
