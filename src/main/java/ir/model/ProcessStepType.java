package ir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessStepType extends BaseEntity {
	private String name;
	private String onAcceptStep;
	private String onRejectStep;
	private Role role;

	@JsonProperty
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty
	public String getOnAcceptStep() {
		return onAcceptStep;
	}

	public void setOnAcceptStep(String onAcceptStep) {
		this.onAcceptStep = onAcceptStep;
	}

	@JsonProperty
	public String getOnRejectStep() {
		return onRejectStep;
	}

	public void setOnRejectStep(String onRejectStep) {
		this.onRejectStep = onRejectStep;
	}

	@JsonProperty
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "ProcessStepType [name=" + name + ", onAcceptStep=" + onAcceptStep + ", onRejectStep=" + onRejectStep
				+ ", role=" + role + "]";
	}

}
