package ir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Role extends BaseEntity {
	private String name;

	@JsonProperty
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
