package ir.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessType extends BaseEntity {
	private String name;
	private ArrayList<ProcessStepType> steps;
	private String startStepUid;

	@JsonProperty
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty
	public ArrayList<ProcessStepType> getSteps() {
		return steps;
	}

	public ProcessStepType getStep(String uid){
		for(ProcessStepType type:steps){
			if(type.getUid().equals(uid))
				return type;
		}
		return null;
	}

	public void setSteps(ArrayList<ProcessStepType> steps) {
		this.steps = steps;
	}

	@JsonProperty
	public String getStartStepUid() {
		return startStepUid;
	}

	public void setStartStepUid(String startStepUid) {
		this.startStepUid = startStepUid;
	}

	@Override
	public String toString() {
		return "ProcessType [name=" + name + ", steps=" + steps + ", startStepUid=" + startStepUid + "]";
	}

	public boolean hasStep(String stepUid) {
		if ("FINISH".equalsIgnoreCase(stepUid))
			return true;
		for (ProcessStepType type : steps) {
			if (stepUid.equals(type.getUid()))
				return true;
		}
		return false;
	}

	public boolean hasFinishStep() {
		for (ProcessStepType type : steps) {
			if ("FINISH".equalsIgnoreCase(type.getOnAcceptStep()))
				return true;
		}
		return false;
	}

}
