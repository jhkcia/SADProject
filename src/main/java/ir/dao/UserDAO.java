package ir.dao;

import org.bson.Document;

import com.mongodb.BasicDBObject;

import ir.model.User;

public class UserDAO extends BaseEntityDAO<User> {
	private static UserDAO instance;

	public static UserDAO getInstance() {
		if (instance == null)
			instance = new UserDAO();
		return instance;
	}

	private static final String ROLE_FIELD = "role";
	private static final String FIRST_NAME_FIELD = "firstName";
	private static final String LAST_NAME_FIELD = "lastName";
	private static final String USER_NAME_FIELD = "username";
	private static final String PASSWORD_FIELD = "password";
	private static final String BIRTH_DATE_FIELD = "birthDate";
	private static final String NATIONAL_ID_FIELD = "nationalId";
	private static final String TOKEN_FIELD = "token";

	@Override
	public Document save(User item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(BIRTH_DATE_FIELD, item.getBirthDate());
		doc.put(FIRST_NAME_FIELD, item.getFirstName());
		doc.put(LAST_NAME_FIELD, item.getLastName());
		doc.put(USER_NAME_FIELD, item.getUsername());
		doc.put(NATIONAL_ID_FIELD, item.getNationalId());
		doc.put(PASSWORD_FIELD, item.getPassword());
		doc.put(ROLE_FIELD, item.getRole().getUid());
		doc.put(TOKEN_FIELD, item.getToken());
		return doc;
	}

	@Override
	public User load(Document doc) {
		User item = new User();
		item.setUid(doc.getString(UID_FIELD));
		item.setBirthDate(doc.getLong(BIRTH_DATE_FIELD));
		item.setFirstName(doc.getString(FIRST_NAME_FIELD));
		item.setLastName(doc.getString(LAST_NAME_FIELD));
		item.setUsername(doc.getString(USER_NAME_FIELD));
		item.setNationalId(doc.getLong(NATIONAL_ID_FIELD));
		item.setPassword(doc.getString(PASSWORD_FIELD));
		item.setRole(RoleDAO.getInstance().getByUid(doc.getString(ROLE_FIELD)));
		item.setToken(doc.getString(TOKEN_FIELD));
		return item;
	}

	public User getByToken(String token) {
		BasicDBObject searchQuery = new BasicDBObject().append(TOKEN_FIELD, token);
		Document res = getOrCreateCollection().find(searchQuery).first();
		if (res == null)
			return null;
		return load(res);
	}

	public User getByUsername(String username) {
		BasicDBObject searchQuery = new BasicDBObject().append(USER_NAME_FIELD, username);
		Document res = getOrCreateCollection().find(searchQuery).first();
		if (res == null)
			return null;
		return load(res);
	}

	public void setToken(String userName, String token) {
		BasicDBObject searchQuery = new BasicDBObject().append(USER_NAME_FIELD, userName);
		BasicDBObject updateQuery = new BasicDBObject().append("$set", new BasicDBObject().append(TOKEN_FIELD, token));
		getOrCreateCollection().updateOne(searchQuery, updateQuery);
	}

	@Override
	public String getCollectionName() {
		return "user";
	}

}
