package ir.dao;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;

import ir.model.Message;
import ir.model.ProcessStepInstance;

public class ProcessStepInstanceDAO extends BaseEntityDAO<ProcessStepInstance> {
	private static final String PROCESS_INSTANCE_FIELD = "processInstance";
	private static final String COST_FIELD = "cost";
	private static final String STATE_FIELD = "state";
	private static final String MESSAGES_FIELD = "messages";
	private static final String CREATION_DATE_FIELD = "creationDate";
	private static final String FINISH_DATE_FIELD = "finishDate";
	private static final String ROLE_FIELD = "role";
	private static final String MESSAGE_TEXT_FIELD = "text";
	private static final String MESSAGE_SENDER_FIELD = "sender";
	private static final String PROCESS_STEP_TYPE_UID_FIELD = "stepType";

	private static ProcessStepInstanceDAO instance;

	public static ProcessStepInstanceDAO getInstance() {
		if (instance == null)
			instance = new ProcessStepInstanceDAO();
		return instance;
	}

	@Override
	public String getCollectionName() {
		return "process_step_instance";
	}

	@Override
	public Document save(ProcessStepInstance item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(PROCESS_INSTANCE_FIELD, item.getProcessInstance());
		doc.put(COST_FIELD, item.getCost());
		doc.put(STATE_FIELD, item.getState());
		doc.put(MESSAGES_FIELD, saveMessages(item.getMessages()));
		doc.put(CREATION_DATE_FIELD, item.getCreationDate());
		doc.put(FINISH_DATE_FIELD, item.getFinishDate());
		doc.put(ROLE_FIELD, item.getRole());
		doc.put(PROCESS_STEP_TYPE_UID_FIELD, item.getProcessStepTypeUid());
		return doc;
	}

	private ArrayList<Document> saveMessages(ArrayList<Message> messages) {
		ArrayList<Document> out = new ArrayList<>();
		
		for (Message m : messages)
			out.add(saveMessage(m));
		return out;

	}

	private Document saveMessage(Message m) {
		Document doc = new Document();
		doc.append(MESSAGE_SENDER_FIELD, m.getSender());
		doc.append(MESSAGE_TEXT_FIELD, m.getText());
		return doc;
	}

	@Override
	public ProcessStepInstance load(Document doc) {
		ProcessStepInstance item = new ProcessStepInstance();
		item.setUid(doc.getString(UID_FIELD));
		item.setCost(doc.getLong(COST_FIELD));
		item.setCreationDate(doc.getLong(CREATION_DATE_FIELD));
		item.setFinishDate(doc.getLong(FINISH_DATE_FIELD));
		item.setProcessInstance(doc.getString(PROCESS_INSTANCE_FIELD));
		item.setProcessStepTypeUid(doc.getString(PROCESS_STEP_TYPE_UID_FIELD));
		item.setRole(doc.getString(ROLE_FIELD));
		item.setState(doc.getString(STATE_FIELD));
		@SuppressWarnings("unchecked")
		ArrayList<Document> steps = (ArrayList<Document>) doc.get(MESSAGES_FIELD);

		item.setMessages(loadMessages(steps));

		return item;
	}

	private ArrayList<Message> loadMessages(ArrayList<Document> steps) {
		ArrayList<Message> out = new ArrayList<>();
		for (Document m : steps) {
			Message msg = new Message();
			msg.setSender(m.getString(MESSAGE_SENDER_FIELD));
			msg.setText(m.getString(MESSAGE_TEXT_FIELD));
			out.add(msg);
		}
		return out;
	}

	public ArrayList<ProcessStepInstance> getOpenedSteps(String roleUid) {
		ArrayList<ProcessStepInstance> outList = new ArrayList<>();
		BasicDBObject searchQuery = new BasicDBObject().append(STATE_FIELD, "OPEN").append(ROLE_FIELD, roleUid);
		MongoCursor<Document> result = getOrCreateCollection().find(searchQuery).iterator();
		while (result.hasNext()) {
			outList.add(load(result.next()));
		}
		return outList;
	}

}
