package ir.dao;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;

import ir.model.ProcessInstance;

public class ProcessInstanceDAO extends BaseEntityDAO<ProcessInstance> {
	private static ProcessInstanceDAO instance;
	private static final String CREATION_DATE_FIELD = "creationDate";
	private static final String FINISH_DATE_FIELD = "finishDate";
	private static final String CURRENT_STEP_FIELD = "currentStepInstanceUid";
	private static final String PROCESS_TYPE_FIELD = "processType";
	private static final String OWNER_FIELD = "owner";
	private static final String STATE_FIELD = "state";

	public static ProcessInstanceDAO getInstance() {
		if (instance == null)
			instance = new ProcessInstanceDAO();
		return instance;
	}

	@Override
	public String getCollectionName() {
		return "process_instance";
	}

	@Override
	public Document save(ProcessInstance item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(CREATION_DATE_FIELD, item.getCreationDate());
		doc.put(FINISH_DATE_FIELD, item.getFinishDate());
		doc.put(CURRENT_STEP_FIELD, item.getCurrentStepInstanceUid());
		doc.put(PROCESS_TYPE_FIELD, item.getProcessType());
		doc.put(OWNER_FIELD, item.getOwner());
		doc.put(STATE_FIELD, item.getState());
		return doc;
	}

	@Override
	public ProcessInstance load(Document doc) {
		ProcessInstance item = new ProcessInstance();
		item.setUid(doc.getString(UID_FIELD));
		item.setCreationDate(doc.getLong(CREATION_DATE_FIELD));
		item.setFinishDate(doc.getLong(FINISH_DATE_FIELD));
		item.setCurrentStepInstanceUid(doc.getString(CURRENT_STEP_FIELD));
		item.setProcessType(doc.getString(PROCESS_TYPE_FIELD));
		item.setOwner(doc.getString(OWNER_FIELD));
		item.setState(doc.getString(STATE_FIELD));
		return item;
	}

	public ArrayList<ProcessInstance> getUserItems(String uid) {
		ArrayList<ProcessInstance> outList = new ArrayList<>();
		BasicDBObject searchQuery = new BasicDBObject().append(OWNER_FIELD, uid);
		MongoCursor<Document> result = getOrCreateCollection().find(searchQuery).iterator();
		while (result.hasNext()) {
			outList.add(load(result.next()));
		}
		return outList;

	}

	public ProcessInstance getByUserAndType(String userUid, String processType) {
		BasicDBObject searchQuery = new BasicDBObject().append(OWNER_FIELD, userUid).append(PROCESS_TYPE_FIELD,
				processType);
		Document result = getOrCreateCollection().find(searchQuery).first();
		if (result == null)
			return null;
		return load(result);

	}

}
