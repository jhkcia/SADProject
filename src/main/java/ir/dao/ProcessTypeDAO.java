package ir.dao;

import java.util.ArrayList;

import org.bson.Document;

import ir.model.ProcessStepType;
import ir.model.ProcessType;

public class ProcessTypeDAO extends BaseEntityDAO<ProcessType> {
	private static final String NAME_FIELD = "name";
	private static final String START_STEP_FIELD = "startStep";
	private static final String STEPS_FIELD = "steps";
	private static final String ON_ACCEPT_FIELD = "onAcceptStep";
	private static final String ON_REJECT_FIELD = "onRejectStep";
	private static final String ROLE_FIELD = "role";

	private static ProcessTypeDAO instance;

	public static ProcessTypeDAO getInstance() {
		if (instance == null)
			instance = new ProcessTypeDAO();
		return instance;
	}

	@Override
	public String getCollectionName() {
		return "process_type";
	}

	@Override
	public Document save(ProcessType item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(NAME_FIELD, item.getName());
		doc.put(START_STEP_FIELD, item.getStartStepUid());
		doc.put(STEPS_FIELD, saveSteps(item.getSteps()));
		return doc;
	}

	private ArrayList<Document> saveSteps(ArrayList<ProcessStepType> items) {
		ArrayList<Document> out = new ArrayList<>();
		for (ProcessStepType type : items)
			out.add(saveStep(type));
		return out;
	}

	private Document saveStep(ProcessStepType item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(NAME_FIELD, item.getName());
		doc.put(ON_ACCEPT_FIELD, item.getOnAcceptStep());
		doc.put(ON_REJECT_FIELD, item.getOnRejectStep());
		doc.put(ROLE_FIELD, item.getRole().getUid());
		return doc;
	}

	@Override
	public ProcessType load(Document doc) {
		ProcessType item = new ProcessType();
		item.setUid(doc.getString(UID_FIELD));
		item.setName(doc.getString(NAME_FIELD));
		item.setStartStepUid(doc.getString(START_STEP_FIELD));
		@SuppressWarnings("unchecked")
		ArrayList<Document> steps = (ArrayList<Document>) doc.get(STEPS_FIELD);
		item.setSteps(loadSteps(steps));
		return item;
	}

	private ArrayList<ProcessStepType> loadSteps(ArrayList<Document> steps) {
		ArrayList<ProcessStepType> out = new ArrayList<>();
		for (Document doc : steps) {
			out.add(loadStep(doc));
		}
		return out;
	}

	private ProcessStepType loadStep(Document doc) {
		ProcessStepType item = new ProcessStepType();
		item.setUid(doc.getString(UID_FIELD));
		item.setName(doc.getString(NAME_FIELD));
		item.setOnAcceptStep(doc.getString(ON_ACCEPT_FIELD));
		item.setOnRejectStep(doc.getString(ON_REJECT_FIELD));
		item.setRole(RoleDAO.getInstance().getByUid(doc.getString(ROLE_FIELD)));
		return item;
	}

}
