package ir.dao;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import ir.model.BaseEntity;
import ir.util.UidGenerator;

public abstract class BaseEntityDAO<T extends BaseEntity> {
	private static final String MONGO_HOST = "localhost";
	private static final int MONGO_PORT = 27017;
	protected static final String UID_FIELD = "uid";

	private static final String MONGO_DB_NAME = "sad_proj";

	private static MongoClient mongoClient;

	public abstract String getCollectionName();

	private static MongoClient getClient() {
		if (mongoClient == null)
			mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);
		return mongoClient;

	}

	private static MongoDatabase getDataBase() {
		return getClient().getDatabase(MONGO_DB_NAME);
	}

	public MongoCollection<Document> getOrCreateCollection() {
		if (!getDataBase().listCollectionNames().into(new ArrayList<String>()).contains(getCollectionName())) {
			getDataBase().createCollection(getCollectionName());
		}
		return getDataBase().getCollection(getCollectionName());

	}

	private BasicDBObject searchByUidQuery(String uid) {
		return searchByFieldQuery(UID_FIELD, uid);
	}

	protected BasicDBObject searchByFieldQuery(String fieldname, String value) {
		return new BasicDBObject().append(fieldname, value);
	}

	public T getByUid(String uid) {
		FindIterable<Document> result = getOrCreateCollection().find(searchByUidQuery(uid));
		Document res = result.first();
		if (res == null)
			return null;
		return load(res);
	}

	public void setToken(String userName, String token) {

	}

	public T AddOrUpdate(T item) {
		String uid = item.getUid();
		if (uid == null || "".equals(uid)) {
			uid = UidGenerator.generateUid();
			item.setUid(uid);
			getOrCreateCollection().insertOne(save(item));
		} else {
			getOrCreateCollection().replaceOne(searchByUidQuery(uid), save(item));
		}
		return getByUid(uid);
	}

	public ArrayList<T> getItems() {
		ArrayList<T> outList = new ArrayList<>();
		MongoCursor<Document> result = getOrCreateCollection().find().iterator();
		while (result.hasNext()) {
			outList.add(load(result.next()));
		}
		return outList;
	}

	public void delete(String uid) {
		getOrCreateCollection().deleteOne(searchByUidQuery(uid));
	}

	public abstract Document save(T item);

	public abstract T load(Document item);

	public void drop() {
		getOrCreateCollection().drop();
	}
}
