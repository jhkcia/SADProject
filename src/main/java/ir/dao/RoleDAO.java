package ir.dao;

import org.bson.Document;

import com.mongodb.client.FindIterable;

import ir.model.Role;

public class RoleDAO extends BaseEntityDAO<Role> {
	private static final String NAME_FIELD = "name";
	private static RoleDAO instance;

	public static RoleDAO getInstance() {
		if (instance == null)
			instance = new RoleDAO();
		return instance;
	}

	@Override
	public String getCollectionName() {
		return "role";
	}

	@Override
	public Document save(Role item) {
		Document doc = new Document();
		doc.put(UID_FIELD, item.getUid());
		doc.put(NAME_FIELD, item.getName());
		return doc;
	}

	@Override
	public Role load(Document doc) {
		Role item = new Role();
		item.setUid(doc.getString(UID_FIELD));
		item.setName(doc.getString(NAME_FIELD));
		return item;
	}

	public Role getByName(String name) {
		FindIterable<Document> result = getOrCreateCollection().find(searchByFieldQuery(NAME_FIELD, name));
		Document res = result.first();
		if (res == null)
			return null;
		return load(res);
	}

}
